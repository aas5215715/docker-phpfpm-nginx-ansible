"""Role testing files using testinfra."""

import pytest
import testinfra

def test_hosts_file(host):
    """Validate /etc/hosts file."""
    f = host.file("/etc/hosts")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"

def test_packages(host):
  pkg = host.package("nginx")
  assert pkg.is_installed
  assert pkg.version.startswith("1.18")