"""Role testing files using testinfra."""

def test_host_reachable(host):
    web = host.addr("51.250.15.235")
    assert web.port(8080).is_reachable
