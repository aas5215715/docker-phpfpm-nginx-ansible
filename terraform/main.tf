terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}



// Set variables
variable "yandex_token" {
    description = "Yandex Cloud security OAuth token"
    default     = "" #generate yours by this https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token
    type = string
    sensitive = true
}
variable "yandex_cloud_id" {
    description = "Yandex Cloud ID where resources will be created"
    default     = ""
    type = string
    sensitive = true
}
variable "yandex_folder_id" {
    description = "Yandex Cloud Folder ID where resources will be created"
    default     = ""
    type = string
}



// Configure Yandex.Cloud provider
provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}



// Create a new instance
resource "yandex_compute_instance" "module-ten" {
  name = "module-ten"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet-m10v2.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

  provisioner "file" {
    source      = "user_data.sh"
    destination = "/tmp/user_data.sh"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/id_ed25519")}"
      host        = "${yandex_compute_instance.module-ten.network_interface.0.nat_ip_address}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/user_data.sh",
      "/tmp/user_data.sh",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/id_ed25519")}"
      host        = "${yandex_compute_instance.module-ten.network_interface.0.nat_ip_address}"
    }
  }
}



// Configure VPC Nework
resource "yandex_vpc_network" "vpc-network-m10v2" {
  name = "vpc-network-m10v2"
}
resource "yandex_vpc_subnet" "vpc-subnet-m10v2" {
  name           = "vpc-subnet-m10v2"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.vpc-network-m10v2.id}"
  v4_cidr_blocks = ["10.241.1.0/24"]
}



// Show internal/external IP
output "internal_ip_address_vm_1" {
  value = "${yandex_compute_instance.module-ten.network_interface.0.ip_address}"
}
output "external_ip_address_vm_1" {
  value = "${yandex_compute_instance.module-ten.network_interface.0.nat_ip_address}"
}